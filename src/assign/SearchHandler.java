package assign;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SearchHandler {

	Logger logger = Logger.getLogger(this.getClass().getName());

	public SearchHandler() {
	}

	public ArrayList<ContentBean> translateToContent(Document doc) {
		NodeList dataNodes = doc.getElementsByTagName("dc");
		ArrayList<ContentBean> dataList = new ArrayList<ContentBean>();

		for (int i = 0; i < dataNodes.getLength(); i++) {
			Node n = dataNodes.item(i);
			NodeList dataElements = n.getChildNodes();
			ContentBean d = new ContentBean();
			d.setContentID(i);

			for (int j = 0; j < dataElements.getLength(); j++) {
				Node e = dataElements.item(j);
				// logger.info(e.getNodeName() + ":" + e.getTextContent());

				if (e.getNodeName().equalsIgnoreCase("title")) {
					d.setTitle(e.getTextContent());

				}
				if (e.getNodeName().equalsIgnoreCase("publisher")) {
					d.setPublisher(e.getTextContent());
				}
				if (e.getNodeName().equalsIgnoreCase("identifier")) {
					d.setIdentifier(e.getTextContent());
				}
				if (e.getNodeName().equalsIgnoreCase("contributor")) {
					d.setContributor(e.getTextContent());
				}
				if (e.getNodeName().equalsIgnoreCase("description")) {
					d.setDescription(e.getTextContent());
				}
				if (e.getNodeName().equalsIgnoreCase("subject")) {
					d.setSubject(e.getTextContent());
				}
				if (e.getNodeName().equalsIgnoreCase("rights")) {
					d.setRights(e.getTextContent());
				}
				if (e.getNodeName().equalsIgnoreCase("coverage")) {
					d.setCoverage(e.getTextContent());
				}

			}

			dataList.add(d);
		}

		return dataList;

	}

}
