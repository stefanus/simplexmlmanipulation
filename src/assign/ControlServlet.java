package assign;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Servlet implementation class ControlServlet
 */
@WebServlet("/parsecontents")
public class ControlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger(this.getClass().getName());

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ControlServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		// CHECK FOR THE INITIAL CART AND DOESN'T EXIST SET ONE UP.
		CartBean cart = (CartBean) request.getSession().getAttribute("cart");
		ServletContext context = getServletContext();
		if (cart == null) {
			cart = new CartBean();
			request.getSession().setAttribute("cart", cart);
		}

		// STORE ITEM INTO THE CART.
		String addToCartFlag = request.getParameter("addToCart");

		@SuppressWarnings("unchecked")
		ArrayList<ContentBean> savedContents = (ArrayList<ContentBean>) context
				.getAttribute("contents");

		if (addToCartFlag != null) {

			String addToCart = request.getParameter("contentID");

			for (int i = 0; i < savedContents.size(); i++) {

				String getID = Integer.toString(savedContents.get(i)
						.getContentID());

				if (getID.equalsIgnoreCase(addToCart)) {

					cart.addToCart(savedContents.get(i));
					break;
				}
			}

			RequestDispatcher rd = request.getRequestDispatcher("/results.jsp");
			rd.forward(request, response);
		}

		// TO VIEW YOUR CART
		String viewCart = request.getParameter("viewCart");
		if (viewCart != null) {
			request.getSession().getAttribute("cart");
			CartBean checkCart = (CartBean) request.getSession().getAttribute(
					"cart");
			int cartSize = checkCart.getSize();

			request.getSession().setAttribute("cartSize", cartSize);
			RequestDispatcher rd = request.getRequestDispatcher("/myCart.jsp");
			rd.forward(request, response);
		}

		// TO DELETE THE SELECTED ITEMS FROM CART>

		String deleteSelected = request.getParameter("deleteSelected");
		String selection[] = request.getParameterValues("selectedContent");
		if (deleteSelected != null) {

			if (selection == null) {
				RequestDispatcher rd = request
						.getRequestDispatcher("/myCart.jsp");
				rd.forward(request, response);
			} else {
				for (int i = 0; i < selection.length; i++) {

					cart.deleteItem(selection[i]);

				}

				request.getSession().setAttribute("cart", cart);
				RequestDispatcher rd = request
						.getRequestDispatcher("/myCart.jsp");
				rd.forward(request, response);
			}

		}

		// PAGINATION BUTTON FOR PREVIOUS AND NEXT
		String previous = request.getParameter("previous");
		String next = request.getParameter("next");
		int currentPage = (Integer) request.getSession().getAttribute(
				"currentPage");
		Integer pageCount = (Integer) request.getSession().getAttribute("pageCount");
		if (previous != null) {

			// GETTING ALL THE DIFFERENT PARAMETER THAT IS SET UP BEFORE HAND.
			currentPage = currentPage - 1;

			ArrayList<ContentBean> showResult = new ArrayList<ContentBean>();
			@SuppressWarnings("unchecked")
			ArrayList<ContentBean> searchResult = (ArrayList<ContentBean>) request
					.getSession().getAttribute("searchResult");
			Integer contentPerPage = (Integer) request.getSession()
					.getAttribute("contentPerPage");

			
			// GET THE RANGE OF THE PREVIOUS ITEM
			int nextItemsRange = (currentPage * contentPerPage);
			int startIndex = nextItemsRange - contentPerPage;
			
			//CHECKER TO HANDLE THE LAST PAGE AND THE LAST SET OF ITEMS OR STANDARD SET
			if(currentPage == pageCount){
				for (int i = nextItemsRange-10; i < searchResult.size(); i++) {
					showResult.add(searchResult.get(i));
				}
			}else{
				for (int i = startIndex; i < nextItemsRange; i++) {
					showResult.add(searchResult.get(i));
				}
			}
			
			// SERVE THE PAGE.
			request.getSession().setAttribute("currentPage", currentPage);
			request.getSession().setAttribute("showResult", showResult);

			RequestDispatcher rd = request.getRequestDispatcher("/results.jsp");
			rd.forward(request, response);
		}

		if (next != null) {
			
			// GETTING ALL THE DIFFERENT PARAMETER THAT IS SET UP BEFORE HAND.
			currentPage = currentPage + 1;

			ArrayList<ContentBean> showResult = new ArrayList<ContentBean>();

			@SuppressWarnings("unchecked")
			ArrayList<ContentBean> searchResult = (ArrayList<ContentBean>) request
					.getSession().getAttribute("searchResult");
			Integer contentPerPage = (Integer) request.getSession()
					.getAttribute("contentPerPage");

			// GET THE RANGE OF THE PREVIOUS ITEM
			int nextItemsRange = (currentPage * contentPerPage);
			int startIndex = nextItemsRange - contentPerPage;

			
			//CHECKER TO HANDLE THE LAST PAGE AND THE LAST SET OF ITEMS OR STANDARD SET
			if(currentPage == pageCount){
				for (int i = nextItemsRange-10; i < searchResult.size(); i++) {
					showResult.add(searchResult.get(i));
				}
			}else{
				for (int i = startIndex; i < nextItemsRange; i++) {
					showResult.add(searchResult.get(i));
				}
			}
			
			// SERVE THE PAGE
			request.getSession().setAttribute("currentPage", currentPage);
			request.getSession().setAttribute("showResult", showResult);

			RequestDispatcher rd = request.getRequestDispatcher("/results.jsp");
			rd.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		ServletContext context = getServletContext();
		InputSource xmlFile = new InputSource(
				context.getResourceAsStream("/WEB-INF/ands_registry_dc.xml"));

		@SuppressWarnings("unchecked")
		ArrayList<ContentBean> savedContents = (ArrayList<ContentBean>) context
				.getAttribute("contents");

		// GETTING ALL PARAMETER FROM THE DIFFERENT POSSIBLE ACTIONS.

		String searchSubmit = request.getParameter("searchSubmit");
		String searchQuery = request.getParameter("searchQuery");
		String contentID = request.getParameter("contentID");

		// TO PARSE THE VERY FIRST BUNCH OF CONTENTS.
		ArrayList<ContentBean> contents = null;
		if (savedContents == null) {

			try {
				DocumentBuilderFactory builderFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder builder = builderFactory.newDocumentBuilder();
				Document doc = builder.parse(xmlFile);
				SearchHandler handler = new SearchHandler();

				contents = handler.translateToContent(doc);
			} catch (Exception e) {
				logger.severe(e.getMessage());
			}

			savedContents = contents;
			context.setAttribute("contents", contents);
		}

		// FOR SEARCHING 

		if (searchSubmit != null) {

			// PROCESS SEARCH QUERY AND RESULT.

			ArrayList<ContentBean> searchResult = null;
			searchResult = doSearch(savedContents, searchQuery);

			// SHOWING RESULT.

			if ((searchResult.size() == 0)
					|| (searchQuery.equalsIgnoreCase(""))) {
				int resultFlag = -1;
				String noResult = "Sorry, no matching datasets found!";
				request.setAttribute("noResult", noResult);
				request.setAttribute("resultFlag", resultFlag);

			}

			request.getSession().setAttribute("searchResult", searchResult);

			// SETTING UP THE FIRST PAGE &
			// FOR PAGINATION SET UP NUMBER OF ITEMS PER PAGE AND PAGE COUNT.

			int currentPage = 1;
			int contentPerPage = 10;
			boolean pageButtonFlag = false;
			int pageCount = (searchResult.size() / contentPerPage) + 1;

			request.getSession().setAttribute("currentPage", currentPage);
			request.getSession().setAttribute("contentPerPage", contentPerPage);
			request.getSession().setAttribute("pageCount", pageCount);

			
			 //System.out.println("search size " + searchResult.size());
			// System.out.println("current page "+ currentPage);
			 //System.out.println("page count "+ pageCount);
			ArrayList<ContentBean> showResult = new ArrayList<ContentBean>();

			if (searchResult.size() > 10) {
				for (int k = 0; k < contentPerPage; k++) {
					showResult.add(searchResult.get(k));
				}
				pageButtonFlag = true;
				request.getSession().setAttribute("showResult", showResult);

			} else {
				request.getSession().setAttribute("showResult", searchResult);
			}

			request.getSession().setAttribute("pageButtonFlag", pageButtonFlag);

			RequestDispatcher rd = request.getRequestDispatcher("/results.jsp");
			rd.forward(request, response);

		}

		// FOR SHOWING MORE DETAILS OF A PARTICULAR CONTENT

		if (contentID != null) {

			ArrayList<ContentBean> details = null;

			details = getMoreDetail(savedContents, contentID);
			request.setAttribute("moreDetail", details);
			request.setAttribute("contentID", contentID);
			RequestDispatcher rd = request.getRequestDispatcher("/detail.jsp");
			rd.forward(request, response);

		}

	}

	// TO GET MORE DETAIL OF AN ITEM
	public ArrayList<ContentBean> getMoreDetail(
			ArrayList<ContentBean> contents, String contentID) {

		ArrayList<ContentBean> result = new ArrayList<ContentBean>();

		for (int i = 0; i < contents.size(); i++) {

			String getID = Integer.toString(contents.get(i).getContentID());

			if (getID.equalsIgnoreCase(contentID)) {
				result.add(contents.get(i));
				break;
			}
		}

		return result;
	}

	// TO SEARCH THROUGH THE DATA BASE
	// 
	public ArrayList<ContentBean> doSearch(ArrayList<ContentBean> contents,
			String searchQuery) {
		ArrayList<ContentBean> result = new ArrayList<ContentBean>();

		for (int i = 0; i < contents.size(); i++) {

			ArrayList<String> subjects = contents.get(i).getSubject();

			for (int j = 0; j < subjects.size(); j++) {

// PATTERN MATCHING FOR WORD CONTAIN IN A STRING.
//				if (Pattern
//						.compile(Pattern.quote(searchQuery),
//								Pattern.CASE_INSENSITIVE)
//						.matcher(subjects.get(j)).find()) {
//
//					result.add(contents.get(i));
//					break;
//				}
				
				if (Pattern
						.compile(("(^|\\s)"+searchQuery+"\\b"),
								Pattern.CASE_INSENSITIVE)
						.matcher(subjects.get(j)).find()) {

					result.add(contents.get(i));
					break;
				}
			}

		}

		return result;
	}

}
