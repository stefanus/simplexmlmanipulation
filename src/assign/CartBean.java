package assign;

import java.util.*;
import java.io.Serializable;

public class CartBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<ContentBean> cart;

	public CartBean() {
		cart = new ArrayList<ContentBean>();
	}

	public void addToCart(ContentBean content) {
		cart.add(content);
	}

	public ArrayList<ContentBean> getCart() {
		return cart;
	}

	public void deleteItem(String id) {
		Integer contentID = Integer.parseInt(id);
		for (int i = 0; i < cart.size(); i++) {

			if (cart.get(i).getContentID().equals(contentID)) {
				cart.remove(i);
			}

		}

	}

	public int getSize() {
		return cart.size();
	}

}
