package assign;

import java.util.ArrayList;

public class ContentBean {

	private ArrayList<String> contributor;
	private ArrayList<String> coverage;

	private ArrayList<String> description;
	private ArrayList<String> identifier;

	private ArrayList<String> rights;
	private ArrayList<String> subject;

	private String title;
	private String publisher;
	// private static AtomicInteger nextID = new AtomicInteger();
	private Integer contentID;

	public ContentBean() {

		this.contentID = 0;
		this.contributor = new ArrayList<String>();
		this.coverage = new ArrayList<String>();
		this.description = new ArrayList<String>();
		this.identifier = new ArrayList<String>();
		this.publisher = "";
		this.rights = new ArrayList<String>();
		this.subject = new ArrayList<String>();
		this.title = "";

	}

	// CONTENT ID FOR IDENTIFIER.
	public void setContentID(Integer contentID) {
		this.contentID = contentID;
	}

	public Integer getContentID() {
		return contentID;
	}

	// CONTRIBUTOR//
	public ArrayList<String> getContributor() {
		return contributor;
	}

	public void setContributor(String contributor) {
		this.contributor.add(contributor);
	}

	// COVERAGE //
	public ArrayList<String> getCoverage() {
		return coverage;
	}

	public void setCoverage(String coverage) {
		this.coverage.add(coverage);
	}

	// DESCRIPTION
	public ArrayList<String> getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description.add(description);
	}

	// IDENTIFIER
	public ArrayList<String> getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier.add(identifier);
	}

	// PUBLISHER
	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	// RIGHTS
	public ArrayList<String> getRights() {
		return rights;
	}

	public void setRights(String rights) {
		this.rights.add(rights);
	}

	// SUBJECT
	public ArrayList<String> getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject.add(subject);
	}

	// TITLE
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
