<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="assign.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="savedContents" class="assign.ContentBean"
	scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Result testing</title>
</head>
<body>
	<h1>Showing Search Result.</h1>
	<a href="/Ass1/"> HOME</a>
	<br />
	<br />
	<table>

		<tr>
		</tr>
		<tr>
			<td>
				<form action='parsecontents' method="post">
					<input type='submit' value='My Cart' name="viewCart">
				</form>
			</td>
			<td>
				<form name="backToSearch" action="search.jsp" method="get">

					<input type="submit" value="Back to Search" name="backToSearch">
				</form>
			</td>
		</tr>

	</table>
	<table>
		<tr>
			<br />
			<form action='parsecontents' method="get">
				Search query: <input type="text" name="searchQuery"> <input
					type='submit' value='Submit' name="searchSubmit">
			</form>
		</tr>


		<c:if test="${resultFlag != -1 }">
			<br />

			<tr>
				<th align="left">Title</th>

			</tr>
			<c:forEach var="result" items="${showResult}" >
				<tr>

					<td><a
						href="/Ass1/parsecontents?contentID=${result.contentID}">${result.title}</a>
					</td>

				</tr>
			</c:forEach>
	</table>

	<c:if test="${pageButtonFlag}">

		<table>
			<tr>
				<c:if test="${currentPage != 1}">
					<form name="previous" action="parsecontents" method="post">

						<input type="submit" value="Previous" name="previous">
					</form>
				</c:if>

				<c:if test="${currentPage != pageCount}">
					<form name="next" action="parsecontents" method="post">

						<input type="submit" value="Next" name="next">
					</form>
				</c:if>

			</tr>
		</table>
	</c:if>
	</c:if>

	<table>

		<tr>
			<td><h4>
					<c:out value="${noResult}" />
				</h4></td>
		</tr>
	</table>

</body>
</html>