<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="assign.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:useBean id="savedContents" class="assign.ContentBean"
	scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>My Cart</title>
</head>
<body>
	<h1>My Cart</h1>
	<a href="/Ass1/">HOME</a>

	<c:if test="${cartSize > 0 }">
		<h4>Tick the selection box to delete any items that you want.</h4>
		<table>

			<form name="deletion" action="parsecontents" method="post">

				<c:forEach var="cart" items="${cart.cart}">
					<tr>
						<td><input type="checkbox" name="selectedContent"
							value="${cart.contentID}"> ${cart.title}</td>
					</tr>
				</c:forEach>


				<tr>
					<td><input type="submit" value="Remove from Cart"
						name="deleteSelected"></td>
				</tr>
			</form>

		</table>
		<table>
			<form name="backToSearch" action="search.jsp" method="get">
				<tr>
					<td><input type="submit" value="Back to Search"
						name="backToSearch"></td>
				</tr>
			</form>
		</table>
	</c:if>

	<c:if test="${cartSize == 0 }">
		<table>
			<tr>
				<h3>Shopping Cart is Empty!</h3>
			</tr>
			<form name="backToSearch" action="search.jsp" method="get">
				<tr>
					<td><input type="submit" value="Back to Search"
						name="backToSearch"></td>
				</tr>
			</form>
		</table>
	</c:if>
</body>
</html>