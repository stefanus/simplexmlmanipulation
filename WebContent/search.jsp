<%-- This is the welcome page and does absolutely nothing other than welcome the user--%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="assign.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome!</title>
</head>
<body>
	<%@ include file="Header.html"%>
	<center>
		<h1>Let's get dirty and search!</h1>
		What's the first search?
		<!--
	Send the user on to the Menu page 
 -->
		<form action='parsecontents' method="get">
			Search query: <input type="text" name="searchQuery"><br>
			<input type='submit' value='Search' name="searchSubmit">
		</form>
		<form action='parsecontents' method="post">
			<input type='submit' value='My Cart' name="viewCart">
		</form>
	</center>
	<%@ include file="footer.jsp"%>
</body>
</html>