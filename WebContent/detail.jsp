<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="assign.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:useBean id="savedContents" class="assign.ContentBean"
	scope="session" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Result testing</title>
</head>
<body>
	<h1>More details</h1>

	<a href="/Ass1/"> HOME</a>

	<table>

		<tr>
			<th></th>
			<td>
				<form name="addCart" action="parsecontents" method="post">
					<input type="hidden" name="contentID" value="${contentID}">
						<input type="submit" value="Add to Cart" name="addToCart">
				</form>
			</td>
		</tr>

		<c:forEach var="detail" items="${moreDetail}">
			<tr>
				<th>Title</th>
				<td>${detail.title}</td>



			</tr>
			<tr>
				<th>Subject</th>
				<td>${detail.subject}</td>

			</tr>
			<tr>
				<th>Contributor</th>
				<td>${detail.contributor}</td>

			</tr>
			<tr>
				<th>Coverage</th>
				<td>${detail.coverage}</td>

			</tr>
			<tr>
				<th>Description</th>
				<td>${detail.description}</td>

			</tr>
			<tr>
				<th>Identifier</th>
				<td>${detail.identifier}</td>

			</tr>

			<tr>
				<th>Publisher</th>
				<td>${detail.publisher}</td>

			</tr>
			<tr>
				<th>Right</th>
				<td>${detail.rights}</td>

			</tr>
		</c:forEach>




	</table>



</body>
</html>